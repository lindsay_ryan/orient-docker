FROM saptech/orientdb_base_build

ENV ORIENTDB_USER orient
ENV ORIENTDB_ROOT_PASSWORD foodcartpark!

#As of Orient C.E. v2.1.8, 4 Jan 2016...
#Install instruction in Orient Documentation are wrong. releases directory is not created
#So here we create sym links for the relevant directories
RUN echo 'Using saptech/orientdb_base_build for maven build' \
	&& echo '- use ORIENTDB_VERSION to control which version is used' \
	&& echo '- this creates a release directory with 2 sym links'

RUN ls -l /opt/orientdb/orientdb/distribution/target/orientdb-community-2.1.8.dir
RUN ls -l /opt/orientdb/orientdb/distribution/target/orientdb-community-2.1.8.dir/orientdb-community-2.1.8

WORKDIR $ORIENTDB_DIR

RUN pwd && ls -l \
	&& mkdir releases \
	&& cd releases \
	&& echo 'Making sym link releases/orientdb-community-'$ORIENTDB_VERSION \
	&& ln -s ../orientdb/distribution/target/orientdb-community-$ORIENTDB_VERSION.dir/orientdb-community-$ORIENTDB_VERSION orientdb-community-$ORIENTDB_VERSION \
	&& echo 'Making sym link releases/latest' \
	&& ln -s orientdb-community-$ORIENTDB_VERSION latest 

#update the working directory to the installed version
WORKDIR $ORIENTDB_DIR/releases/latest

RUN echo 'set working directory= '$ORIENTDB_DIR'/releases/latest. Points to'$PWD 
RUN ls -l
RUN chmod 755 bin/*.sh
RUN chmod -R 777 config

#replace placeholders for ORIENTDB_DIR and ORIENTDB_USER
RUN echo 'setting environmental vars for directory and user in orientdb.sh'
RUN sed -i '/ORIENTDB_DIR=/c\ORIENTDB_DIR="'$ORIENTDB_DIR'"' $ORIENTDB_DIR/releases/latest/bin/orientdb.sh
RUN sed -i '/ORIENTDB_USER=/c\ORIENTDB_USER="'$ORIENTDB_USER'"' $ORIENTDB_DIR/releases/latest/bin/orientdb.sh

VOLUME ["/orientdb/backup", "/orientdb/databases", "/orientdb/config"]
 
CMD ["bin/server.sh"]
